
function el(type, classes, text, parent, attributes){
    let el = document.createElement(type);

    if(!!classes){
        if(typeof(classes) == "string"){
            el.classList.add(classes);
        }else if(typeof(classes) == "object"){
            Object.values(classes).forEach(c => {
                el.classList.add(c);
            })
        }
    }
    
    if(!!text){
        el.innerText = text;
    }

    if(!!parent){
        parent.appendChild(el);
    }

    if(!!attributes){
        for(let attr of Object.keys(attributes)){
            el.setAttribute(attr, attributes[attr]);
        }
    }

    return el;
}

function $(id){
    return document.getElementById(id);
}