function gen_value_entry(value) {
    switch (typeof(value)){
        case "object":
            if(Array.isArray(value)){
                let ul = document.createElement("div");
                ul.classList.add("list", "grid")
                for(const v of value){
                    let li = document.createElement("div");
                    li.appendChild(gen_value_entry(v));
                    ul.appendChild(li);
                }
                return ul;
            }else{
                return gen_object_table(value);
            }
        default:
            return document.createTextNode("" + value);
    }
}

function gen_object_table(o) {
    let table = document.createElement("div");
    table.classList.add("table", "grid");
    for(const [key, value] of Object.entries(o)){
        let label = document.createElement("div");
        label.classList.add("tab_key");
        label.textContent = key;
        table.appendChild(label);
        table.appendChild(gen_ifd_entry(key, value));
    }

    return table;
}

function gen_ifd_entry(key, o) {
    if(typeof(o) == "object"){
        gen_object_table(o)
    }else{

        if(ICON_TAGNAMES[key]){

        }
    }
}

function render_one_ifdo_stat(key, stats) {
    let container = document.createElement("div");
    if(
        IFDO_TAGS.core.header[key] ||
        IFDO_TAGS.core.items[key] ||
        IFDO_TAGS.capture.tags[key] ||
        IFDO_TAGS.content.tags[key]

    ){
        container.classList.add("specced");
    }
    if(ICON_TAGNAMES[key]){
        let thumb_container = document.createElement("div");

        Object.entries(stats.variants).forEach(([value, count]) => {
            let iconpath = "logos/" + key + "_" + value + ".svg";
            let im_el = document.createElement("img");
            im_el.src = iconpath;
            im_el.classList.add("icon");
            im_el.title = `"${value}", applies to ${count} file${count==1?"":"s"}.`
            thumb_container.appendChild(im_el);
        });

        container.appendChild(thumb_container);
    }else{
        container.classList.add("table", "grid");
        if(stats.float_parseable){
            if(Object.keys(stats.variants).length > 3){
                let el_stats = document.createElement("div");
                el_stats.textContent =
                    `Minimum: ${stats.minimum}, Maximum: ${stats.maximum}, Average: ${stats.average}, Standard Deviation: ${stats.standard_deviation}.`;
                el_stats.classList.add("twocol");
                container.appendChild(el_stats);
            }else{
                Object.entries(stats.variants).forEach(([value, count]) => {
                    let el_variant = document.createElement("div");
                    el_variant.textContent = value;
                    let el_count = document.createElement("div");
                    el_count.textContent = count;
                    el_count.classList.add("count");
                    container.appendChild(el_variant);
                    container.appendChild(el_count);
                });
            }
        }else{
            Object.entries(stats.variants).slice(0, 3).forEach(([value, count]) => {
                let el_variant = document.createElement("div");
                el_variant.textContent = value;
                let el_count = document.createElement("div");
                el_count.textContent = count;
                el_count.classList.add("count");
                container.appendChild(el_variant);
                container.appendChild(el_count);
            });
            if(Object.keys(stats.variants).length > 3){
                let el_others = document.createElement("div");
                el_others.textContent = `and ${Object.keys(stats.variants).length - 3} other variant${Object.keys(stats.variants).length - 3 > 1 ? "s":""}.`;
                el_others.classList.add("twocol", "count");
                container.appendChild(el_others);
            }
        }
    }
    if(stats.not_existing){
        let el_not_existing = document.createElement("div");
        el_not_existing.textContent = `${stats.not_existing} file${stats.not_existing == 1 ? "": "s"} without this field.`;
        el_not_existing.classList.add("twocol", "count");
        container.appendChild(el_not_existing);
    }
    return container;

}

function render_flattened_ifdo_stats(stats, unflatten_map) {

    let core_header_fields = {};
    let core_item_fields = {};

    let capture_fields = {};
    let content_fields = {};

    let custom_fields = {};

    function gen_object_table(o) {
        let table = document.createElement("div");
        table.classList.add("table", "grid");
        for(const [key, value] of Object.entries(o)){
            let label = document.createElement("div");
            label.classList.add("tab_key");
            if(
                IFDO_TAGS.core.header[key] ||
                IFDO_TAGS.core.items[key] ||
                IFDO_TAGS.capture.tags[key] ||
                IFDO_TAGS.content.tags[key]
        
            ){
                label.setAttribute("title","Attribute key conforms to iFDO specification")
                label.classList.add("specced");
            }
            label.textContent = key;
            table.appendChild(label);
            if(typeof(value) == "object"){
                table.appendChild(gen_object_table(value));
            }else{
                table.appendChild(render_one_ifdo_stat(key, stats[value]))
            }
        }
    
        return table;
    }

    for(const [key, value] of Object.entries(unflatten_map)){
        if (IFDO_TAGS.core.header[key]) {
            core_header_fields[key] = value;
        }else if (IFDO_TAGS.core.items[key]) {
            core_item_fields[key] = value;
        }else if (IFDO_TAGS.capture.tags[key]) {
            capture_fields[key] = value;
        }else if (IFDO_TAGS.content.tags[key]) {
            content_fields[key] = value;
        }else{
            custom_fields[key] = value;
        }
    }

    let fields_container = document.createElement("div");
    let heading_core = document.createElement("h3");
    heading_core.textContent = "iFDO Core";
    let heading_header = document.createElement("h4");
    heading_header.textContent = "Header";
    let header_fields = gen_object_table(core_header_fields);
    fields_container.appendChild(heading_core);
    fields_container.appendChild(heading_header);
    fields_container.appendChild(header_fields);

    let heading_items = document.createElement("h4");
    heading_items.textContent = "Items";
    let items_fields = gen_object_table(core_item_fields);
    fields_container.appendChild(heading_items);
    fields_container.appendChild(items_fields);

    let heading_capture = document.createElement("h3");
    heading_capture.textContent = "iFDO Capture";
    let el_capture_fields = gen_object_table(capture_fields);
    fields_container.appendChild(heading_capture);
    fields_container.appendChild(el_capture_fields);

    let heading_content = document.createElement("h3");
    heading_content.textContent = "iFDO Content";
    let el_content_fields = gen_object_table(content_fields);
    fields_container.appendChild(heading_content);
    fields_container.appendChild(el_content_fields);

    let heading_custom = document.createElement("h3");
    heading_custom.textContent = "Additional (User-Defined) Fields";
    let el_custom_fields = gen_object_table(custom_fields);
    fields_container.appendChild(heading_custom);
    fields_container.appendChild(el_custom_fields);

    return fields_container;
}