
class Formatting{

    static createIFDOIcon(tag, value){
        let container = el("div", "vstack", null, null);
        let iconpath = "logos/" + tag.tagname + "_" + value + ".svg";
        let _icon = el("img", "icon", null, container, {"src": iconpath});
        let _value = el("div", null, value, container);
        return container;
    }
    
    static formatIFDOValue(tag, value){
        if(ifdo_helpers.ICON_TAGSET.has(tag.tagname)){
            return this.createIFDOIcon(tag, value);
        }else{
            if(ifdo_helpers.isArrayType(tag.tagtype) || Array.isArray(value)){
                let container = el("div", "array", null, null);
                for(let entry of value){
                    container.appendChild(this.formatIFDOValue(
                        {
                            path: tag.path, 
                            tagname: tag.tagname + "[" + entry + "]",
                            tagtype: ifdo_helpers.getArrayBasetype(tag.tagtype)
                        }, 
                        entry
                    ));
                }
                return container;
            }else{
                switch(tag.tagtype){
                    case "string":
                        return el("div", null, value, null);
                    case "float":
                        return el("div", null, value, null);
                    case "object":
                    case "user": //TODO: Special handling for ORCID IDs
                        let parent = el("div", "blocktable", null, null);
                        for(let key of Object.keys(value)){
                            let keyel = el("div", null, key, parent);
                            let valueel = this.formatIFDOValue({path: tag.path, tagname: tag.tagname + "." + key, tagtype: this.guessTagtype(value[key])}, value[key]);
                            parent.appendChild(valueel);
                        }
                        return parent;
                }
            }
        }
    }

    static guessTagtype(value){
        let isArray = Array.isArray(value);
        if(isArray){
            value = value[0];
        }
        let arrayEnd = isArray ? "[]" : "";
        switch(typeof(value)){
            case "object":
                return "object" + arrayEnd;
            case "number":
                return "float" + arrayEnd;
            default:
                return "string" + arrayEnd;
        }
    }
    
    static formatObject(parent, obj){
        for(let key of Object.keys(obj)){
                   
    
        }
    }
}
