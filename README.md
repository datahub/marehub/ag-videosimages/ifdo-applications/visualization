# Introduction

A web application that can turn an iFDO yaml file into a HTML web page and/or PDF.

## Example data
- iFDO yaml file behind handle server: https://data.geomar.de/downloads/20.500.12085/f840644a-fe4a-46a7-9791-e32c211bcbf5 (@ifdo) -> wrong field names!!!!
- image data report: https://cloud.geomar.de/s/8Cca3pPSmmxmyWy

### TODO
- Use Docker container, (flask, Python)
- A web interface to provide an iFDO via URL or file upload
- Visualize the iFDO content on an HTML page (plot coordinates, plot annotations, show example image, plot iFDO fields, show iFDO icons, ...)
- Enable PDF download
