/*
 * Calculate statistical information on the flattened iFDO.
 */
function stats(flattened_ifdo) {

    let stat_pool = {};
    for(const [name, data] of Object.entries(flattened_ifdo)){
        for(const [field, value] of Object.entries(data)){
            if(stat_pool[field]){
                if(stat_pool[field].variants[value]){
                    stat_pool[field].variants[value] += 1;
                }else {
                    stat_pool[field].variants[value] = 1;
                }
            }else{
                stat_pool[field] = {"variants": {}}
                stat_pool[field].variants[value] = 1;
            }
        }
    }

    for(const field of Object.keys(stat_pool)){
        for(const [name, data] of Object.entries(flattened_ifdo)){
            if (!data[field]){
                if(stat_pool[field]["not_existing"]){
                    stat_pool[field]["not_existing"] += 1;
                }else{
                    stat_pool[field]["not_existing"] = 1;
                }           
            }
        }
    }
    for(const [field, stats] of Object.entries(stat_pool)){
        is_float_parseable = true;
        is_date_parseable = true;
        for(variant of Object.keys(stats.variants)){
            is_float_parseable = is_float_parseable && !isNaN(variant);
            is_date_parseable = is_date_parseable && !isNaN(new Date(variant));
        }
        stat_pool[field]["float_parseable"] = is_float_parseable;
        stat_pool[field]["date_parseable"] = is_date_parseable && !is_float_parseable;
    }
    // Add more statistical calculations here...
    for(const [field, stats] of Object.entries(stat_pool)){
        if(stats.float_parseable && Object.keys(stats.variants).length > 1){
            let [avg, total] = Object.entries(stats.variants).reduce(
                ([an, n], [val, amt]) => [an + parseFloat(val) * amt, n + amt],
                [0, 0]
            );
            avg /= total;
            stat_pool[field]["average"] = avg;
            let variance = Object.entries(stats.variants).reduce(
                (a, [val, amt]) => a + Math.pow(avg - parseFloat(val), 2) * amt,
                0
            ) / total;
            stat_pool[field]["variance"] = variance;
            stat_pool[field]["standard_deviation"] = Math.sqrt(variance);
            let [min, max] = Object.keys(stats.variants).reduce(
                ([mi, ma], val) => [Math.min(mi, val), Math.max(ma, val)],
                [Infinity, -Infinity]
            );
            stat_pool[field]["minimum"] = min;
            stat_pool[field]["maximum"] = max;
        }
    }

    return {
        "count": Object.entries(flattened_ifdo).length,
        "stats": stat_pool
    }
}

/*
 *  Find a safe prefix/separator char for iFDO field name flattening, by going through the trees and checking for the highest
 *  character code found in any key. The safe character is then determined by incrementing the highest found code by one.
 */
function safe_path_separator(ifdo) {
    let header = ifdo["image-set-header"];
    let bodies = ifdo["image-set-items"];
    function highest_char_code(obj){
        let cc0 = "a".charCodeAt(0);
        if(Array.isArray(obj)){
            cc0 = Math.max(cc0, 57);
            obj.forEach(o => {
                if(typeof(0) == "object"){
                    cc0 = Math.max(highest_char_code(o), cc0);
                }
            });
        } else {
            if(obj == undefined){
                return 0;
            }
            for(const [key, value] of Object.entries(obj)){
                for(var i = 0; i < key.length; i++){
                    let cc = key.charCodeAt(i);
                    cc0 = Math.max(cc0, cc);
                }
                if(typeof(value) == "object"){
                    cc0 = Math.max(highest_char_code(value), cc0);
                }
            }
        }
        return cc0;
    }
    let cmax = highest_char_code(header);
    for(const o of Object.values(bodies)){
        cmax = Math.max(cmax, highest_char_code(o[0]));
    }
    return String.fromCharCode(cmax + 1);
}

/*
 *  Flatten the iFDO structure. The YAML structure is repackaged into an object of objects. The trunk-level key is the
 *  file name as present in the image-set-items object. The second level uses flattened keys generated from the header-
 *  and item object structure by joining them after prefixing with a safe separator character. The header values are
 *  applied first and then overwritten if specified again for an individual item. The safe separator is also returned.
 */
function flatten_structure(ifdo) {
    let separator = safe_path_separator(ifdo);

    function flatten_object(obj, sep) {
        if(typeof(obj) == "object"){
            let tmp = {};
            if(Array.isArray(obj)){
                obj.forEach((v, i) => {
                    for(const[k, w] of Object.entries(flatten_object(v, sep))){
                        tmp[sep + i + k] = w;
                    }
                });
            }else{
                for(const[j, v] of Object.entries(obj)){
                    for(const[k, w] of Object.entries(flatten_object(v, sep))){
                        tmp[sep + j + k] = w;
                    }
                }
            }
            return tmp;
        }else{
            return {"": obj}
        }
    }

    let header_entries_default = flatten_object(ifdo["image-set-header"], separator);

    let flattened_items = {}

    for(const [key, a] of Object.entries(ifdo["image-set-items"])){
        let value = flatten_object(a[0], separator);
        let tmp = {};
        for(const [k, v] of Object.entries(header_entries_default)){
            tmp[k] = v;
        }
        for(const [k, v] of Object.entries(value)){
            tmp[k] = v;
        }
        flattened_items[key] = tmp;
    }

    return[flattened_items, separator];
}

/*
 *  From the keys of a flattened object, recreate the object's structure by splitting the paths.
 *  Then the generated object can be used to recreate the encoded hierarchy, while also providing
 *  the flattened path for access to the statistical data.
 */
function create_unflatten_map(flattened, sep) {
    let tmp = {};
    for(const key of Object.keys(flattened)){
        let key_parts = key.split(sep).slice(1);
        let level = tmp;
        for(let p of key_parts.slice(0, -1)){
            if(!level[p]){
                level[p] = {};
            }
            level = level[p];
        }
        level[key_parts[key_parts.length - 1]] = key;
    }
    return tmp;
}