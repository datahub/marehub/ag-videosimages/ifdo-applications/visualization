
const IFDO_TAGS = {
    core: {
        header: {
            "image-set-name": "string",
            "image-set-uuid": "string",
            "image-set-handle": "string",
            "image-set-ifdo-version": "string"
        },
        items: {
            "image-datetime": "string",
            "image-latitude": "float",
            "image-longitude": "float",
            "image-depth": "float",
            "image-altitude": "float",
            "image-coordinate-reference-system": "string",
            "image-coordinate-uncertaintymeters": "float",
            "image-context": "string",
            "image-project": "string",
            "image-event": "string",
            "image-platform": "string",
            "image-sensor": "string",
            "image-uuid": "string",
            "image-hash-sha256": "string",
            "image-pi": "user",
            "image-creators": "user[]",
            "image-license": "string",
            "image-copyright": "string",
            "image-abstract": "string"
        }
    },
    capture: {
        tags: {
            "image-acquisition": "string",
            "image-quality": "string",
            "image-deployment": "string",
            "image-navigation": "string",
            "image-scale-reference": "string",
            "image-illumination": "string",
            "image-pixel-magnitude": "string",
            "image-marine-zone": "string",
            "image-spectral-resolution": "string",
            "image-capture-mode": "string",
            "image-area-square-meter": "float",
            "image-meters-above-ground": "float",
            "image-acquisition-settings": "object",
            "image-camera-yaw-degrees": "float",
            "image-camera-pitch-degrees": "float",
            "image-camera-roll-degrees": "float",
            "image-overlap-fraction": "float",
            "image-datetime-format": "string",
            "image-camera-pose": "object",
            "image-camera-housing-viewport": "object",
            "image-flatport-parameters": "object",
            "image-domeport-parameters": "object",
            "image-camera-calibration-model": "object",
            "image-photometric-calibration": "object",
            "image-objective": "string",
            "image-target-environment": "string",
            "image-target-timescale": "string",
            "image-spatial-contraints": "string",
            "image-temporal-constraints": "string",
            "image-fauna-attraction": "string",
            "image-time-synchronisation": "string",
            "image-item-identification-scheme": "string",
            "image-curation-protocol": "string"
        }
    },
    content: {
        tags: {
            "image-entropy": "float",
            "image-particle-count": "float",
            "image-average-color": "float[]",
            "image-mpeg7-colorlayout": "float[]",
            "image-mpeg7-colorstructure": "float[]",
            "image-mpeg7-dominantcolor": "float[]",
            "image-mpeg7-edgehistogram": "float[]",
            "image-mpeg7-homogeneoustexture": "float[]",
            "image-mpeg7-scalablecolor": "float[]",
            "image-annotation-labels": "object[]",
            "image-annotation-creators": "user[]",
            "image-annotations": "object[]",
        }
    }
}

const ICON_TAGNAMES = {
    "image-license": ["CC-BY", "CC-0"],
    "image-acquisition": ["slide", "video", "photo"],
    "image-quality": ["raw", "product", "processed"],
    "image-deployment": ["exploration", "sampling", "stationary", "survey", "experiment", "mapping"],
    "image-navigation": ["beacon", "satellite", "transponder", "reconstructed"],
    "image-scale-reference": ["calibrated camera", "laser marker", "optical flow", "3D camera"],
    "image-illumination": ["artificial", "sun", "mixed"],
    "image-resolution": ["dm", "hm", "m", "cm", "dam", "μm", "mm", "km"],
    "image-marine-zone": ["watercolumn", "atmosphere", "seasurface", "laboratory", "seafloor"],
    "image-spectral-resolution": ["grayscale", "multi-spectral", "rgb", "hyper-spectral"],
    "image-capture-mode": ["timer-and-manual", "manual", "timer"],
    "image-annotation-geometry-types": ["polygon", "whole-image", "bounding-box", "single-pixel"],
    "image-annotation-creator-types": ["non-expert", "AI", "expert", "crowd-sourced"],
    "image-annotation-label-types": ["geology", "biology", "operation", "garbage"]
}

let ifdo_helpers = {
    IFDO_TAGS,
    ICON_TAGNAMES, 
    ICON_TAGSET: new Set(Object.keys(ICON_TAGNAMES)),
    getPrettyName: function (tagname){
        let name = tagname;
        if(name.startsWith("image-")){
            name = name.substring(6);
        }
        name = name.replace(/\-/g, " ");
        let re = /(\b[a-z](?!\s))/g;
        name = name.replace(re , c => c.toUpperCase());
        return name;
    },
    isArrayType: function (typename){
        return typename.endsWith("[]");
    },
    getArrayBasetype: function (typename){
        return typename.replace("[]", "");
    },
    getAllTags: function*(){
        let scopes = ["core", "capture", "content"];
        let subscopes = ["header", "items", "tags"];
        for(let scope of scopes){
            if(!IFDO_TAGS.hasOwnProperty(scope)){
                continue;
            }
            let cursor = IFDO_TAGS[scope];
            for(let subscope of subscopes){
                if(!cursor.hasOwnProperty(subscope)){
                    continue;
                }else{
                    cursor = cursor[subscope];
                    for(let key of Object.keys(cursor)){
                        let tag = {
                            "path": scope + "." + subscope,
                            "tagname": key,
                            "tagtype": cursor[key]
                        };
                        yield tag;
                    }
                }

            }
        }
        return;
    }

};









function getHTMLForName(tag){

}