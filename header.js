
function parseHeader(ifdo){
    let nameEl = document.getElementById("header-image-set-name");
    let header = ifdo["image-set-header"];
    nameEl.innerText = header["image-set-name"];

    let header_el = document.getElementById("header");

    let header_information_block = el("div", "block", null, header_el);
    let header_information_title = el("h6", null, "Header Information", header_information_block);

    let header_information_table = el("div", "blocktable", null, header_information_block);

    for(let tag of ifdo_helpers.getAllTags()){
        if(header.hasOwnProperty(tag.tagname)){
            let tag_title = el("div", null, ifdo_helpers.getPrettyName(tag.tagname), header_information_table);
            let tag_value = Formatting.formatIFDOValue(tag, header[tag.tagname]);
            header_information_table.appendChild(tag_value);
        }
    }
    

    header_el.style.display = "block";

}